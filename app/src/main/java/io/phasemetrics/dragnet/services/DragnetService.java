package io.phasemetrics.dragnet.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import io.phasemetrics.dragnet.DragnetAidlInterface;

/**
 * Created by sando on 4/28/16.
 */
public class DragnetService extends Service {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final DragnetAidlInterface.Stub pluginBinder = new DragnetAidlInterface.Stub() {
        @Override
        public void doNothing() throws RemoteException {
            System.out.println("What a drag...");
        }
    };
}
